import java.util.HashMap;

public class InsertOnlySampler{
    private kwiseHash hash;
    private int range;
    private long minHashValue;
    private int minValueItem;

    public InsertOnlySampler(int range){
        hash = new kwiseHash(5);
        this.range = range;
        minHashValue = range;
    }

    public void update(int item, int increment) {
        long h = hash.h_basic(item,range);
        if (h<minHashValue){
            minValueItem = item;
            minHashValue = h;
        }

    }

    public int output() {

        return minValueItem;

    }

    public static void main(String[] args){
        int repeatTime = 1000;
        int range = 200;
        int universe = 10;
        int entry = 100;
        int ru =entry/universe;
        Runtime r = Runtime.getRuntime();
        r.gc();
        long startMem = r.freeMemory();
        InsertOnlySampler[] ios = new InsertOnlySampler[repeatTime];
        for (int i=0;i<repeatTime;i++){
            ios[i] = new InsertOnlySampler(range);
        }
        HashMap<Integer,Integer> hm = new HashMap<>();
        //10*200 + 20*50 + 50*40

        for(int i=0;i<universe;i++){
            hm.put(i,0);
        }
        Stopwatch w = new Stopwatch();
        for (int i=0;i<ru;i++){
            for(int j=0;j<repeatTime;j++){
                for(int k=0;k<universe;k++){
                    ios[j].update(k,1);
                }
            }
        }
        long usedMem = startMem-r.freeMemory();
        System.out.println("memused"+usedMem+"bytes");
        System.out.print("time used"+w.elapsedTime()+"\n");
        for(int i=0;i<repeatTime;i++){
            hm.replace(ios[i].output(),hm.get(ios[i].output())+1);
        }
        for (int i:hm.keySet()){
            System.out.println(hm.get(i));
        }

    }
}
