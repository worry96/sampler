import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

public class RandomStream {

    public ArrayList<Object> RS;
    public HashMap<Object,Integer> streamMap;

    public RandomStream(){
        RS = new ArrayList<Object>();
        streamMap = new HashMap<>();
    }

    public void add(Object o, int times){
        if (times<=0)
            return;
        for (int i=0;i<times;i++){
            RS.add(o);
        }
    }

    public void addRandomInt(int num, int lowerBound, int upperBound){
        for (int i=0;i<num;i++){
            int r = StdRandom.uniform(lowerBound,upperBound);
            RS.add(r);
        }

    }

    public void addRandomDouble(int num, double lowerBound, double upperBound){
        for (int i=0;i<num;i++){
            double d = StdRandom.uniform(lowerBound,upperBound);
            RS.add(d);
        }
    }

    public static String randomString(double avgLength, double stdLength){
        long length = Math.round(StdRandom.gaussian(avgLength,stdLength));
        length = Math.max(1,length);
        StringBuilder sb = new StringBuilder();
        for(int j=0;j<length;j++){
            sb.append(Character.toChars('a'+StdRandom.uniform(26)));
        }
        return sb.toString();

    }

    public void addRandomWord(int num, double avgLength, double stdLength){
        for (int i=0;i<num;i++){
            RS.add(randomString(avgLength,stdLength));
        }
    }

    public void addRealText(String path){
        try{
            Scanner StreamObject = new Scanner(new FileInputStream(path));
            while(StreamObject.hasNext()){
                String[] record = StreamObject.nextLine().split(" ");
                for (String s: record){
                    RS.add(s);
                }
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
        System.out.println(RS.size());
    }

    public void addRandomTuple(int num){
        for(int i=0;i<num;i++){
            Object tuple[] = new Object[3];
            tuple[0] = StdRandom.uniform(1000);
            tuple[1] = StdRandom.uniform(0.0,1000.0);
            tuple[2] = randomString(4,2);
            RS.add(tuple);
        }
    }

    public void clear(){
        RS.clear();
        streamMap.clear();
    }

    public void shuffle() {
        Collections.shuffle(RS);
    }

    public void establishMap(){
        streamMap = new HashMap<>();
        for (Object o:RS) {
            if (streamMap.containsKey(o)) {
                streamMap.replace(o, streamMap.get(o) + 1);
            } else streamMap.put(o, 1);
        }
    }

    public int getAccurateFre(Object o){
        if (streamMap==null)
            return -1;
        else return streamMap.get(o)==null?0:streamMap.get(o);
    }
}
