import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Scanner;

public class oneSparseRecovery {
    // a large prime random chosen form a list
    private int p;
    // a number chosen in range of (0,q)
    private long q;
    protected long tau;
    protected int F1;
    protected long U;
    protected long V;


    public oneSparseRecovery(){
        //int []primeList= {769};
        int[] primeList = {2121223373,1782933881,962337197,505065571,818243917,312313501
            ,1084713541,64759433,762323461,1013720459,414803159,98244923,712334849};
        p = primeList[StdRandom.uniform(primeList.length)];

        q = (long)StdRandom.uniform(p);

        // is p itself a suitable choice?
        F1 = 0;
        tau = 0;
        U = 0;
        V = 0;
    }

    public void update(int item, int increment) {
        //calculate (sum of fi*q^i)mod p
        long result = increment * helper(q,item,p);
        tau = (tau+result)%p;
        F1 += increment;
        U += item*increment;
        V += item*item*increment;
    }


    public boolean oneSparce() {
        //test if tau == (F1 * q^(U/F1))mod p
        if (F1 == 0)
            return false;
        //System.out.println(U);
        //System.out.println(tau);
        //System.out.println((F1*helper(q,U/F1,p))%p);

        if (U%F1 != 0)
            return false;
        else if ((tau-(F1*helper(q,U/F1,p)))%p==0)
                return true;
        else return false;
    }

    public boolean isZero(){
        return F1==0&&V==0;
    }


    public Integer recover(){
        if (oneSparce())
            return (int)U/F1;
        else return null;
    }

    public static long helper(long x, long y, int p){
        //calcul x^y mod p
        long result = 1;
        while(y>0){
            if (y%2 == 1){
                result = (result*x)%p;
                y = y-1;
            }
            x = (x*x)%p;
            y = y/2;
        }
        return result;
    }

    public static void main(String[] args){
        //test if the code is correct
        int k=0;
        Stopwatch w = new Stopwatch();
            oneSparseRecovery osr = new oneSparseRecovery();
            try {
                Scanner s = new Scanner(new FileInputStream("test.txt"));
                int n = Integer.parseInt(s.nextLine());;
                while(s.hasNextLine()){
                    String[] str = s.nextLine().split(" ");
                    osr.update(Integer.parseInt(str[0]),Integer.parseInt(str[1]));
                }
                if(osr.oneSparce())
                    k++;
                s.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //System.out.print(osr.recover());

        System.out.println(w.elapsedTime()+"seconds");
        System.out.println(k);
    }



}
