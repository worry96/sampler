import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class DynamicSampler {
    //universe size
    private int n;
    //range of the hash function
    private long range;
    private kwiseHash h;
    private int kSRlength;
    private kSparseRecovery[] kSR;
    public DynamicSampler(int n){
        this.n = n;
        //map item into a n*n*n size table to avoid collision
        this.range = n*n*n;

        //kSR level parameter
        kSRlength = (int) Math.ceil(Math.log((double)n)/Math.log(2.0));
        kSR = new kSparseRecovery[kSRlength+1];
        for (int i=0;i<=kSRlength;i++){
            //sparsity(kSR) = 12*log(1/epsilon)
            //if we choose epsilon = 1/10000, sparsity is 12*4 = 48;
            kSR[i] = new kSparseRecovery(16,48);
        }
        h = new kwiseHash(48/2);
    }
    public void update(int item, int increment){
        int l=0;
        while((range/Math.pow(2,l)>=h.h2u(item,range))&&(l<=kSRlength)){
            kSR[l].update(item,increment);
            l++;
        }
    }

    public Integer output(){
        int l = 0;
        HashMap<Integer,Integer> A = new HashMap<>();
        while(l<=kSRlength){
            //System.out.print(l);
            if(kSR[l].sSparseTest()){
                long minHashValue = range;
                int result = 0;
                for(int item: kSR[l].recover().keySet()){
                    if(h.h2u(item,range) < minHashValue){
                        minHashValue = h.h2u(item,range);
                        result = item;
                    }
                }
                return result;
            }
            l++;
        }
        return null;
    }


    public static void main(String[] args){
        int repeatTime = 1000;
        int range = 200;
        int universe = 10;
        int entry = 10;
        int ru =entry/universe;
        Runtime r = Runtime.getRuntime();
        r.gc();
        long startMem = r.freeMemory();
        DynamicSampler[] ds = new DynamicSampler[repeatTime];
        for (int i=0;i<repeatTime;i++){
            ds[i] = new DynamicSampler(range);
        }
        HashMap<Integer,Integer> hm = new HashMap<>();
        for(int i=0;i<universe;i++){
            hm.put(i,0);
        }
        Stopwatch w = new Stopwatch();
        for (int i=0;i<ru;i++){
            for(int j=0;j<repeatTime;j++){
                for(int k=0;k<universe;k++){
                    ds[j].update(k,1);
                }
            }
        }

        long usedMem = startMem-r.freeMemory();
        System.out.println("memused"+usedMem+"bytes");
        System.out.print("time used"+w.elapsedTime()+"\n");
        for(int i=0;i<repeatTime;i++){
                hm.replace(ds[i].output(),hm.get(ds[i].output())+1);
        }
        for (int i:hm.keySet()){
            System.out.println(hm.get(i));
        }


        //debug test
        /**
        DynamicSampler[] ds = new DynamicSampler[1000];
        for (int i=0;i<1000;i++){
            ds[i] = new DynamicSampler(20);
        }
        RandomStream rs = new RandomStream();
        rs.add(100,100);
        rs.add(20,20);
        rs.add(5,5);
        rs.add(1,1);
        rs.add(1000,1000);
        rs.shuffle();

        for (Object o:rs.RS){
            for(int i=0;i<1000;i++)
                ds[i].update((int)o,1);
        }
        HashMap<Integer,Integer> hm = new HashMap<>();
        hm.put(100,0);
        hm.put(20,0);
        hm.put(1000,0);
        hm.put(1,0);
        hm.put(5,0);
        for(int i=0;i<1000;i++){
            if (ds[i].output()!=null)
                hm.replace(ds[i].output(),hm.get(ds[i].output())+1);
        }
        for (int i:hm.keySet()){
            System.out.println(hm.get(i));
        }**/

    }
}
