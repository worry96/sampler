import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class kSparseRecovery {
    private int d;
    private int sparsity;
    private kwiseHash[] hash;
    private oneSparseRecovery[][] blankets;
    private HashMap<Integer,Integer> items;
    private boolean isZero;
    private boolean isFail;
    private long totalFrequency;
    public kSparseRecovery(int d, int sparsity){
        this.d = d;
        this.sparsity = sparsity;
        hash = new kwiseHash[d];
        for (int i=0;i<d;i++){
            hash[i] = new kwiseHash(5);
        }
        blankets = new oneSparseRecovery[d][sparsity*2];
        for (int i=0;i<d;i++)
            for (int j=0;j<sparsity*2;j++)
                blankets[i][j] = new oneSparseRecovery();
         isZero = false;
         isFail = false;
         totalFrequency = 0;
    }

    public void update(int item, int increment){
        for (int i=0;i<d;i++){
            int a = hash[i].h2u(item,2*sparsity);
            blankets[i][a].update(item,increment);
        }
        totalFrequency += increment;
    }

    public boolean sSparseTest(){
        //use U/F1 to represent the frequency of items in blanket
        items = new HashMap<>();
        for (int i=0;i<d;i++){
            for (int j=0;j<2*sparsity;j++){
                oneSparseRecovery blanket = blankets[i][j];
                if (blanket.oneSparce()){
                    items.put((int)(blanket.U/blanket.F1), blanket.F1);
                    //System.out.println(blanket.U/blanket.F1);
                }
            }
        }
        //first test if it is correctly retrieved.
        long checkSum = 0;
        for (int key:items.keySet()){
            checkSum+=items.get(key);
        }

        if (checkSum != totalFrequency){
            //not correctly retrieved
            items.clear();
            isFail = true;
            return false;
        }

        if (items.size()==0){
            items.clear();
            isZero = true;
            return false;
        }
        if (items.size()>sparsity){
            //more than sparsity
            items.clear();
            isFail = true;
            return false;
        }
        return true;
    }

    public HashMap<Integer,Integer> recover(){
        if (!sSparseTest())
            return null;
        else return items;
    }

    public static void main (String[] args){
        int repeat = 500;
        int d = 15;
        kSparseRecovery[] ksr = new kSparseRecovery[repeat];

        //test if the code is correct
        int k=0;
        int wrongreport = 0;
        Stopwatch w = new Stopwatch();
        try {
            Scanner s = new Scanner(new FileInputStream("test.txt"));
            int sp = Integer.parseInt(s.nextLine());;
            for (int i=0;i<repeat;i++){
                ksr[i]=new kSparseRecovery(d,sp);
            }
            while(s.hasNextLine()){
                String[] str = s.nextLine().split(" ");
                int item = Integer.parseInt(str[0]);
                int increment = Integer.parseInt(str[1]);
                for(int i=0;i<repeat;i++){
                    ksr[i].update(item,increment);
                }
            }
            for(int i=0;i<repeat;i++){
                if(ksr[i].sSparseTest()){
                    k++;
                    if(ksr[i].recover().size()!=201)
                        wrongreport++;
                }
            }
            s.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(w.elapsedTime()+"seconds");
        System.out.println(k +" "+ wrongreport);
    }

}
