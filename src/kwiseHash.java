// COMP90056 Assignment A 2019s2

// This file is modified to support long type range.
//
import java.math.BigInteger;
import java.util.Random;

import static java.lang.Math.pow;


public class kwiseHash
{
    private static Random r;
    private long p = (long)pow(2,61)-1;
    private int k;
    private long[] a;


    public kwiseHash(int k)
    {
        a = new long[k];
        if(k<2)
            k=2;
        if (r==null)
            r = new Random();
        this.k = k;
        for(int i=0;i<k-1;i++){
            a[i] = r.nextLong()%(p-1)+1;
        }
        a[k-1]=r.nextLong()%p;
    }
    public long h2u(int x,long range)
    {
        BigInteger result = BigInteger.valueOf(a[0]);

        for(int i=1;i<k;i++){
            result = result.multiply(BigInteger.valueOf(x));
            result = result.add(BigInteger.valueOf(a[i]));
            result = result.mod(BigInteger.valueOf(p));
        }
        result = result.mod(BigInteger.valueOf(range));
        long r = result.longValue();
        return r>=0?r:r+range;
    }

    public long h_basic(Object key,long domain) {
        // domain should be something like 0x0fffffff
        int key_int = key.hashCode();
        return h2u(key_int, domain);
    }

    public int h2u(int x, int range){
        BigInteger result = BigInteger.valueOf(a[0]);

        for(int i=1;i<k;i++){
            result = result.multiply(BigInteger.valueOf(x));
            result = result.add(BigInteger.valueOf(a[i]));
            result = result.mod(BigInteger.valueOf(p));
        }
        result = result.mod(BigInteger.valueOf(range));
        int r = result.intValue();
        return r>=0?r:r+range;
    }
}

