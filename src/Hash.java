// COMP90056 Assignment A 2019s2

// This file is modified to support long type range.
//
import java.math.BigInteger;
import java.util.Random;

import static java.lang.Math.pow;


public class Hash
{
    private static Random r;
    private long p = (long)pow(2,61)-1;
    private long a,b;


    public Hash()
    {
        if (r==null)
            r = new Random();
        a = r.nextLong()%(p-1)+1;
        b = r.nextLong()%(p);
    }
    public long h2u(int x,long range)
    {
        BigInteger prod = BigInteger.valueOf(a);
        prod = prod.multiply(BigInteger.valueOf(x));
        prod = prod.add(BigInteger.valueOf(b));
        prod = prod.mod(BigInteger.valueOf(p));
        prod = prod.mod(BigInteger.valueOf(range));
        int r = prod.intValue();
        return r>=0?r:r+range;
    }

    public long h_basic(Object key,long domain) {
        // domain should be something like 0x0fffffff
        int key_int = key.hashCode();
        return h2u(key_int, domain);
    }

    public int h2u(int x, int range){
        BigInteger prod = BigInteger.valueOf(a);
        prod = prod.multiply(BigInteger.valueOf(x));
        prod = prod.add(BigInteger.valueOf(b));
        prod = prod.mod(BigInteger.valueOf(p));
        prod = prod.mod(BigInteger.valueOf(range));
        int r = prod.intValue();
        return r>=0?r:r+range;
    }
}

